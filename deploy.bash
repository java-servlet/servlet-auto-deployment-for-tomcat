#!/bin/bash 

# ver 0.2  

default_filename=deploy.war 
serverlocation=/usr/local/tomcat-9/webapps/
warlocation=build/libs/

if cd $warlocation; then { # changing directory to build/libs/
    if ls *.war; then {  
        mv *.war $default_filename # deleting previous build files 
        bash /usr/local/tomcat-9/bin/catalina.sh stop # stop tomcat service  
        mv -v $default_filename $serverlocation # moved build files to server 
        bash /usr/local/tomcat-9/bin/catalina.sh start # restarting tomcat services 
    } fi
} fi

exit 0 # exit command 
