# Servlet Auto Deployment for Tomcat

**Variables**
```bash
default_filename=deploy.war 
serverlocation=/usr/local/tomcat-9/webapps/
warlocation=build/libs/
```

**How to use** <br>
place `deploy.bash` file into your root project directory. 
```bash
$ bash deploy.bash
```

> Note : It is created to work with intellij Idea 

Farhan Sadik